import { RvcrmPage } from './app.po';

describe('rvcrm App', () => {
  let page: RvcrmPage;

  beforeEach(() => {
    page = new RvcrmPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
